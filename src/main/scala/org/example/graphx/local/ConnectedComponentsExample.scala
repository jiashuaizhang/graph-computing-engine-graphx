package org.example.graphx.local

import org.apache.spark.graphx.GraphLoader
import org.apache.spark.sql.SparkSession

object ConnectedComponentsExample {

  def main(args: Array[String]): Unit = {

    // 创建一个SparkSession
    val spark = SparkSession
      .builder
      .master("local")
      .appName("graphx-connectedComponent")
      .getOrCreate()
    val sc = spark.sparkContext

    // 读取图数据文件
    val graph = GraphLoader.edgeListFile(sc, "data/graphx/followers.txt")
    // 调用连通图算法
    val cc = graph.connectedComponents().vertices
    // 将节点映射为人物名称
    val users = sc.textFile("data/graphx/users.txt").map { line =>
      val fields = line.split(",")
      (fields(0).toLong, fields(1))
    }
    val ccByUsername = users.join(cc).map {
      case (id, (username, cc)) => (username, cc)
    }
    // 在控制台输出结果
    println(ccByUsername.collect().mkString("\n"))

    spark.stop()
  }

}
