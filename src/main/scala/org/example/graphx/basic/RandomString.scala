package org.example.graphx.basic

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf

import scala.util.Random

object RandomString {

  def main(args: Array[String]): Unit = {
    // 创建 SparkSession
    val spark = SparkSession.builder()
      .appName("Generate Random String")
      .master("local[*]")  // 设置 Spark 的运行模式
      .getOrCreate()

    // 定义生成随机字符串的 UDF
    val randomStringUDF = udf(() => Random.alphanumeric.take(10).mkString)

    println(Random.alphanumeric.take(10).mkString)

    // 创建一个包含随机字符串的 DataFrame
    val df = spark.range(5).select(randomStringUDF().as("random_string"))

    // 显示 DataFrame 内容
    df.show()
  }

}
