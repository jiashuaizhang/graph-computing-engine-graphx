# 图计算引擎graphx的应用

GraphX 是 Apache Spark 的图计算组件，它将图计算和分布式数据处理结合在一起。GraphX 提供了高性能的分布式图计算能力，并且能够与 Spark 的其他组件（如Spark SQL、Spark Streaming）进行集成。它支持大规模图数据的处理和常见的图算法，且具有灵活的数据操作接口。



Nebula Graph Algorithm集成了graphx中的相关算法，实现了数据存储、检索和分析的一体化操作。



**软件版本**

- spark：2.4.4
- scala：2.11.12
- nebula graph algorithm：3.0.0

> 测试使用的代码会在spark集群上运行



## 算法

- PageRank算法：根据点之间的关系（边）计算点的相关性和重要性，通常使用在搜索引擎页面排名中。如果一个网页被很多其他网页链接，说明这个网页比较重要（PageRank 值较高）。
  - 输入：图数据
  - 输出：节点id、节点的PageRank值
- Degree算法：用于查找图中的流行点。度中心性测量来自点的传入或传出（或两者）关系的数量，具体取决于关系投影的方向。一个点的度越大就意味着这个点的度中心性越高，该点在网络中就越重要。
  - 输入：图数据
  - 输出：节点id、节点的双向度中心性、节点的出方向度中心性、节点的入方向度中心性
- Louvain算法：基于模块度的社区发现算法，该算法在效率和效果上都表现较好，并且能够发现层次性的社区结构。
  - 输入：图数据
  - 输出：节点id、节点的标签
- ConnectedComponent算法：能够将图中的节点划分为不同的连通分量，可以用于识别社交网络中的社区结构、网络中的强连通分量、道路网中的连通路段等应用场景。
  - 输入：图数据
  - 输出：节点id、节点的标签
- TriangleCount算法：用于统计图中三角形个数。三角形越多，代表图中节点关联程度越高，组织关系越严密。
  - 输入：图数据
  - 输出：节点id、节点的三角形数量
- ClusteringCoefficient算法：用于计算图中节点的聚集程度。聚类系数指示了一个节点的邻居节点之间形成连边的概率，介于 0 到 1 之间。当该值接近 0 时，表示节点之间没有形成紧密的连接；当该值接近 1 时，表示节点的邻居节点之间形成了更多的连边，存在较高的聚集性。
  - 输入：图数据
  - 输出：节点id、节点的聚集系数



## 程序架构

```
project
	├──data：数据目录
	└──src
		└──scala
			└──org.example
				├──graphx：对graphx组件包含算法的应用
				|	└──local：在本地环境中调用算法
				└──nebula：对nebula graph集成算法的应用
					├──local：在本地环境中调用算法
					├──cluster：在spark集群环境中调用算法
					└──deloly：在spark集群环境中，通过参数传递调用nebula graph集成的算法
```



在local包、cluster包中的文件输入、结果输出都是定义好的，deploy包中算法调用是可以传递参数的，略显复杂，下面展开说明一下：

共有3中参数，分别对应输入、调用的算法和输出

- 参数type
  - file：表示以文本输入数据
  - jdbc： 表示从数据库输入数据
  - txt：表示从参数中接收数据
- 参数algo: 表示要调用算法，包括Degree、PageRank、ClusteringCoefficient、Louvain、LPA、SCC、TriangleCount、cc
- 参数result
  - type：表示计算结果的格式
    - top：返回逆序排列后的靠前的节点，number为节点数量
    - file：表示计算结果写入的路径

**参数示例**

- 图数据在文本中

  1. 将文本数据上传至hdfs中

  2. 准备参数

     ```
     {
         "type": "file",
         "algo": "PageRank",
         "params": {
             "dataPath": "F:/home/spark/string_data.csv",
             "resultPath": "F:/home/result/string_data_result.csv"
         },
         "result": {
             "type": "top",
             "number": 5
         }
     }
     ```

  3. 使用spark-submit命令

     ```
     spark-submit --master yarn --deploy-mode cluster --driver-memory 2G --driver-cores 1 --executor-memory 2G --executor-cores 2  --num-executors 3 --class org.example.nebula.deploy.RunNebulaAlgo xxx.jar "{\"type\": \"file\", 	\"algo\": \"PageRank\", 	\"params\": { 		\"dataPath\": \"F:/home/spark/string_data.csv\", 		\"resultPath\": \"F:/home/result/string_data_result.csv\" 	}, 	\"result\": { 		\"type\": \"top\", 		\"number\": 5 	} }"
     ```

- 图数据在mysql表中

  1. 准备参数

     ```
     {
         "type": "jdbc",
         "algo": "PageRank",
         "params": {
             "url": "jdbc:mysql://localhost:3306/shop",
             "driver": "com.mysql.jdbc.Driver",
             "user": "root",
             "password": "root",
             "dbtable": "test_spark",
             "srcColumn": "src_account_id",
             "dstColumn": "dst_account_id"
         },
         "result": {
             "type": "file",
             "resultPath": "/home/zjs/string_data_result_xx.csv"
         }
     }
     ```

  2. 使用spark-submit命令

     ```
     spark-submit --master yarn --deploy-mode cluster --driver-memory 2G --driver-cores 1 --executor-memory 2G --executor-cores 2  --num-executors 3 --class org.example.nebula.deploy.RunNebulaAlgo xxx.jar {\"type\": \"jdbc\",     \"algo\": \"PageRank\",     \"params\": {         \"url\": \"jdbc:mysql://localhost:3306/shop\",         \"driver\": \"com.mysql.jdbc.Driver\",         \"user\": \"root\",         \"password\": \"root\",         \"dbtable\": \"test_spark\",         \"srcColumn\": \"src_account_id\",         \"dstColumn\": \"dst_account_id\"     },     \"result\": {         \"type\": \"file\",         \"resultPath\": \"/home/zjs/string_data_result_xx.csv\"     } }
     ```

- 图数据在传递的参数中
  1. 准备参数

     ```
     {
         "type": "text",
         "algo": "Degree",
         "params": {
             "src": ["a", "c", "1", "1", "2", "2", "3", "3", "2"],
             "dst": ["b", "d", "4", "5", "3", "4", "4", "5", "5"]
         },
         "result": {
             "type": "top",
             "number": 5
         }
     }
     ```

  2. 使用spark-submit命令

     ```
     spark-submit --master yarn --deploy-mode cluster --driver-memory 2G --driver-cores 1 --executor-memory 2G --executor-cores 2  --num-executors 3 --class org.example.nebula.deploy.RunNebulaAlgo xxx.jar {\"type\": \"text\",     \"algo\": \"Degree\",     \"params\": {         \"src\": [\"a\", \"c\", \"1\", \"1\", \"2\", \"2\", \"3\", \"3\", \"2\"],         \"dst\": [\"b\", \"d\", \"4\", \"5\", \"3\", \"4\", \"4\", \"5\", \"5\"]     },     \"result\": {         \"type\": \"top\",         \"number\": 5     } }
     ```



> 除了程序自定义的参数之外，spark其他的参数都为：--master yarn --deploy-mode cluster --driver-memory 2G --driver-cores 1 --executor-memory 2G --executor-cores 2  --num-executors 3

